<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Actions;
use App\Entity\ActionList;

class ActionsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $actionList = new ActionList();
        

        for ($i=0; $i < 5; $i++) { 
            $action = new Actions();
            $action->setName("Action ${i}");
            $action->setValue(3);
            $action->setComment("This is a comment");
        
            $actionList->addAction($action);

            $manager->persist($action);
        }

        $manager->persist($actionList);
        $manager->flush();
    }
}
