<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use App\Repository\ActionListRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\ActionList;
use App\Entity\Actions;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/actions", name="actions")
 */

class ActionsController extends Controller
{
    private $serializer;


    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function all(ActionListRepository $repo)
    {
        $repo = $this->getDoctrine()->getRepository(Actions::class);
        $list = $repo->findAll();
        $json = $this->serializer->serialize($list, "json");

        return JsonResponse::fromJsonString($json);

    }


    /**
     * @Route("/", methods={"POST"})
     */
    public function add(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $actionList = $this->serializer->deserialize($content, Actions::class, "json");

        $manager->persist($actionList);
        $manager->flush();

        $data = $this->serializer->normalize($actionList, null, ['attributes' =>['id', 'total']]);

        $response = new Response($this->serializer->serialize($actionList, "json"));
        return $response;
    }


    /**
     * @Route("/{id}", methods={"DELETE"})
     */
    public function del(Actions $action)
    {   
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($action);
        $manager->flush();

        return new JsonResponse([], 204);

    }

    /**
     * @Route("/{id}", methods={"PUT"})
     */
    public function upd(Request $request, ActionList $actionList)
    {
        $manager = $this->getDoctrine()->getManager();

        $body = $this->serializer->deserialize($request->getContent(),
                                                    Actions::class, 
                                                    "json");
        $action->setContent($body->getContent());                                                    
        $action->setDate($body->getDate());                                                    
        $action->setName($body->getName());
        $action->setComment($body->getComment());

        $manager->flush();

        $json = $this->serializer->serialize($action, "json");

        return JsonResponse::fromJsonString($json);

    }


}
