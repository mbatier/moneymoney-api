<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\SerializerInterface;
use App\Repository\MemoRepository;
use App\Entity\Memo;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/**
 * @Route("/memo", name="memo")
 */

class MemoController extends Controller
{
    private $serializer;


    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function all(MemoRepository $repo)
    {
        $repo = $this->getDoctrine()->getRepository(Memo::class);
        $memo = $repo->findAll();
        $json = $this->serializer->serialize($memo, "json");

        return JsonResponse::fromJsonString($json);

    }


    /**
     * @Route("/", methods={"POST"})
     */
    public function add(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $memo = $this->serializer->deserialize($content, Memo::class, "json");

        $manager->persist($memo);
        $manager->flush();

        $data = $this->serializer->normalize($memo, null, ['attributes' =>['id', 'name', 'value', 'comment', 'date']]);

        $response = new Response($this->serializer->serialize($memo, "json"));
        return $response;
    }



    /**
     * @Route("/{id}", methods={"DELETE"})
     */
    public function del(Memo $memo)
    {   
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($memo);
        $manager->flush();

        return new JsonResponse([], 204);

    }

    /**
     * @Route("/{id}", methods={"PUT"})
     */
    public function upd(Request $request, Memo $memo)
    {
        $manager = $this->getDoctrine()->getManager();

        $body = $this->serializer->deserialize($request->getContent(),
                                                    Memo::class, 
                                                    "json");
                                                   
        $memo->setValue($body->getValue());                                                    
        $memo->setName($body->getName());
        $memo->setComment($body->getComment());
        $memo->setDate($body->getDate());

        $manager->flush();

        $json = $this->serializer->serialize($memo, "json");

        return JsonResponse::fromJsonString($json);

    }

}
