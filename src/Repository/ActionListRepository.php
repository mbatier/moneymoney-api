<?php

namespace App\Repository;

use App\Entity\ActionList;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ActionList|null find($id, $lockMode = null, $lockVersion = null)
 * @method ActionList|null findOneBy(array $criteria, array $orderBy = null)
 * @method ActionList[]    findAll()
 * @method ActionList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActionListRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ActionList::class);
    }

//    /**
//     * @return ActionList[] Returns an array of ActionList objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ActionList
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
