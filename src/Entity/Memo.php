<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MemoRepository")
 */
class Memo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $comment;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $value;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(?int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate($date): self
    {
        if ($date instanceof \DateTime) {
            $this->date = $date;
        } elseif (is_string($date)) {
            $this->date = \DateTime::createFromFormat("d/m/Y", $date);
            if (!$this->date) {
                $this->date = new \DateTime($date);
            }
        } elseif (is_int($date)) {
            $this->date = new \DateTime();
            $this->date->setTimestamp($date / 1000);
        } elseif (is_array($date)) {
           $this->date = new \DateTime($date["year"]."-".$date["month"]."-".$date["day"]);
        }
        if (!$this->date instanceof \DateTime) {
            throw new \InvalidArgumentException("Date must be d/m/Y or timestamp or DateTime");
        }

        return $this;
    }
}
